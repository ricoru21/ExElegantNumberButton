ElegantNumberButton
-

Una pequeña libreria de Android para realizar un contador de números con botones de incremento y decremento.

También se puede tener precionado el bonton (-/+) para realizar incremento y decremento automaticamente.

###### Captura pantalla
![](Home.png)

###### Metodos integrados a la libreria
<pre>
	button.setOnLongClickListener();
	button.setOnTouchListener();
</pre>

###### Atributos xml adicionados
Atributos agregados, para poder utilizar desde el layout.xml
<pre>
	attr name="downInterval" format="integer"
	attr name="downMillisegundos" format="integer"
</pre>

![](Layout.png)

> Creditos ashik94vc - ElegantNumberButton: [Enlace de Referencia](https://github.com/ashik94vc/ElegantNumberButton)